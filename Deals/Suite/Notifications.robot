*** Settings ***
Resource               ../base/base.txt
Resource               ../Page/page_navigation.txt
Resource               ../Flow/User.txt
Library                Selenium2Library

*** Test Cases ***
mark_all
         [Documentation]         mark all as read
         open
         I Agree
         Sign In         mengli.wang@razer.com         12345678
         #点击message图标
         click         xpath=//span[@class='iconfont icon-btn-email deals-message']
         Sleep         2
         #点击mark all as reade
         click         xpath=//button[@class='btn btn-viewmore allread-btn fr']
         #获取success弹窗信息
         wait visible         xpath=//div[@class='deals-note']/span
         ${success_text}         Get Text         xpath=//div[@class='deals-note']/span
         Should Be Equal         ${success_text}         Success!
         Close All Browsers

delete_message
         [Documentation]         验证删除message
         open
         I Agree
         Sign In         mengli.wang@razer.com         12345678
         #点击message图标
         click         xpath=//span[@class='iconfont icon-btn-email deals-message']
         Sleep         2
         #获取信息
         wait visible         xpath=//div[@class='title']
         ${title}         Get Text         xpath=//div[@class='title']
         #获取时间
         wait visible         xpath=//span[@class='time']
         ${time}         Get Text         xpath=//span[@class='time']
         ${message1}         catenate         ${title}         ${time}
         #点击删除按钮
         Mouse Over         xpath=//div[@class='col-lg-12 col-md-12 col-sm-12 col-xs-12 store-col']
         wait visible         xpath=//span[@class='iconfont icon-delete']
         click         xpath=//span[@class='iconfont icon-delete']
         sleep         5
         ${title2}         Get Text         xpath=//div[@class='title']
         ${time2}         Get Text         xpath=//span[@class='time']
         ${message2}         catenate         ${title2}         ${time2}
         Should Not Be Equal         ${message1}         ${message2}
         close
