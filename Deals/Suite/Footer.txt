*** Settings ***
Library           Selenium2Library
Resource          ../base/base.txt
Resource          ../Flow/User.txt

*** Test Cases ***
FAQ
    open
    click    xpath=//div[@class='help-wrap']/a/span[2]
    sleep    2
    Assert Url    https://deals.razer.com/faq
    close

Legal notices
    open
    sleep    3
    click    xpath=//div[@class='links-wrap ib']/a[1]
    sleep    2
    ${title}    Get Text    xpath=//*[@id="legalModal"]/div/div[1]/span
    Should Be Equal    ${title}    Legal Notices
    click    xpath=//*[@id="legalModal"]/div/div[1]/div
    close

feedback
    open
    Sign In    mengli.wang@razer.com    12345678
    sleep    2
    click    xpath=//a[@id='globalFb']/span[2]
    Press Keys    xpath=//*[@id="globalFbModal"]/div/div[2]/table/tbody/tr[2]/td[2]/input    rcdtest
    Press Keys    xpath=//*[@id="globalFbModal"]/div/div[2]/table/tbody/tr[3]/td[2]/textarea    rcdtest description
    click    xpath=//div[@class='md-footer fb-modal-footer']/button[2]
    close
    feedback admin
    ${title}    Get Text    xpath=//*[@id="result_list"]/tbody/tr[1]/th/a
    #Should Be Equal    ${title}    rcdtest
    close

download
    open
    click    xpath=//div[@class='platform-info dropup']/button
    ${download_url}    get_attr    xpath=//div[@class='platform-info dropup open']/div/a[1]    href
    Should Be Equal    ${download_url}    http://rzr.to/cortex-download
    close
