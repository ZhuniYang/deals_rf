# coding = utf-8
import difflib
class mytool():
    # games页面的view more xpath formate
    # 参数1为Genre
    # 2为Special
    # 3为Price
    # 4为Distributors
    def view_more_xpath_formate(self, n):
        xpath = 'xpath=//a[@data-readmore-toggle="rmjs-{num}"]'
        xpath_new = xpath.format(num=n)
        return xpath_new

    # 点击filters下面的固定值
    # category为分类，比如genre，special,price等
    # n为分类下的第几项
    def click_filters_fixed_item(self, category, n):
        item_xpath = "xpath=//div[@class='checkbox-field readmore {category}']/div[@class='field-wrap']//label[{num}]//span"
        item_xpath_new = item_xpath.format(category=category, num=n)
        return item_xpath_new

    # 比较两个数的大小
    # num1>=num2返回True
    # 小于返回False
    def compare_num(self, num1, num2):
        num1=float(num1)
        num2=float(num2)
        if (num1 >= num2):
            return True
        else:
            return False
     #判断两个字符串的相似度
    def get_equal_rate(self,str1,str2):
        return difflib.SequenceMatcher(None,str1,str2).quick_ratio()