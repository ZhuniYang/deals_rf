*** Settings ***
Resource               ../base/base.txt
Resource               ../Page/page_change.txt
Resource               ../Page/page_navigation.txt
Resource               ../Page/page_games.txt
Resource               ../Page/page_discovery.txt

*** Test Cases ***
change region cn
         [Documentation]         验证区域切换cn
         open
         Click change
         Click change region
         Change region cn
         Click apply
         sleep         1
         ${region}         Get current region
         Should Contain         ${region}         CHINA
         close

change language cn
         [Documentation]         验证语言切换cn
         open
         Click change
         Click change language
         Change language cn
         Click apply
         sleep         1
         ${discovery}         Get discovery text
         Should Be Equal         ${discovery}         发现
         close

change currency cn
         [Documentation]         验证汇率切换cn
         open
         Click change
         Click change currency
         Change currency cn
         Click apply
         sleep         1
         ${new_price}         get random price new
         Should Contain         ${new_price}         ¥
         close
