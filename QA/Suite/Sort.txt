*** Settings ***
Test Setup             BuiltIn.Sleep         5
Test Timeout           3 minutes
Resource               ../base/base.txt
Resource               ../Page/page_navigation.txt
Resource               ../Flow/Sort.txt
Resource               ../Page/page_games.txt
Library                ../MaxineLib/mytool.py
Resource               ../Flow/Games.txt
Resource               ../Flow/User.txt
Resource               ../Flow/Change.txt

*** Test Cases ***
games sort save%
         [Documentation]         验证games页面sort-save%功能
         #随机生成两个index 获取两个item的discount进行比较
         open
         change us
         Click Games
         click games sort item         3
         sleep         10
         ${index_pre}         evaluate         random.randint(0,12)         random
         ${index_late}         evaluate         random.randint(13,25)         random
         sleep         3
         ${discount_pre}         get discount         ${index_pre}
         ${discount_late}         get discount         ${index_late}
         ${bool}         compare_num         ${discount_pre}         ${discount_late}
         Should Be True         ${bool}
         close

games sort save$
         [Documentation]         验证games页面sort-save$功能
         #随机生成两个index 获取两个item的save$进行比较
         open
         change us
         Click Games
         click games sort item         4
         sleep         5
         ${index_pre}         evaluate         random.randint(0,12)         random
         ${index_late}         evaluate         random.randint(13,24)         random
         sleep         3
         ${save_pre}         get save         ${index_pre}
         ${save_late}         get save         ${index_late}
         ${bool}         compare_num         ${save_pre}         ${save_late}
         Should Be True         ${bool}
         close

games sort Price(low to high)
         [Documentation]         验证games页面sort-low to high功能
         #随机生成两个index 获取两个item的price_no_discount进行比较
         open
         change us
         Click Games
         click games sort item         5
         ${index_pre}         evaluate         random.randint(0,12)         random
         ${index_late}         evaluate         random.randint(13,24)         random
         sleep         3
         ${price_pre}         get non-discount price         ${index_pre}
         ${price_late}         get non-discount price         ${index_late}
         ${bool}         compare_num         ${price_late}         ${price_pre}
         Should Be True         ${bool}
         close

games sort Price(high to low)
         [Documentation]         验证games页面sort-high to low功能
         open
         change us
         Click Games
         click games sort item         6
         sleep         5
         ${price_list}         get price list
         ${price_list_copy}         Copy List         ${price_list}
         Sort List         ${price_list_copy}
         Reverse List         ${price_list_copy}
         Should Be Equal         ${price_list}         ${price_list_copy}
         close

bundles sort Price(low to high)
         [Documentation]         验证BUNDLES页面sort-low to high功能
         open
         i_agree
         change us
         Click Bundles
         click bundles sort item         2
         sleep         2
         ${tier1_price_list}         get tier1 price
         ${tier1_price_list_pre}         get tier1 price
         Sort List         ${tier1_price_list_pre}
         log         ${tier1_price_list_pre}
         ${flag}         Run Keyword If         ${tier1_price_list}==${tier1_price_list_pre}         Set Variable         True
         ...         ELSE         Set Variable         False
         Should Be True         ${flag}
         close

bundles sort Price(high to low)
         [Documentation]         验证BUNDLES页面sort-high to low功能
         open
         i_agree
         change us
         Click Bundles
         click bundles sort item         3
         sleep         2
         ${tier1_price_list}         get tier1 price
         ${tier1_price_list_pre}         get tier1 price
         Sort List         ${tier1_price_list_pre}
         Reverse List         ${tier1_price_list_pre}
         log         ${tier1_price_list_pre}
         ${flag}         Run Keyword If         ${tier1_price_list}==${tier1_price_list_pre}         Set Variable         True
         ...         ELSE         Set Variable         False
         Should Be True         ${flag}
         close

wishlist sort addtime(new to old)
         [Documentation]         验证Wishlist页面Sort - Add time(new to old)
         [Timeout]         3 minutes
         open
         I Agree
         Sign In         zhuni.yang@razer.com         Razer123
         sleep         10
         change us
         Click Games
         sleep         1
         ${title}         add random game to wishlist
         sleep         2
         Click Wishlist
         click wishlist sort item         1
         sleep         2
         ${wishlist_first_title}         get wishlist first title
         Should Be Equal         ${title}         ${wishlist_first_title}
         close

wishlist sort save%
         open
         I Agree
         Sign In         zhuni.yang@razer.com         Razer123
         sleep         10
         change us
         Click Wishlist
         sleep         1
         click wishlist sort item         2
         sleep         5
         ${list}         get wishlist discount list
         ${list_copy}         Copy List         ${list}
         Sort List         ${list_copy}
         Reverse List         ${list_copy}
         Should Be Equal         ${list}         ${list_copy}
         close

wishlist sort save$
         open
         I Agree
         Sign In         zhuni.yang@razer.com         Razer123
         sleep         10
         change us
         sleep         1
         Click Wishlist
         sleep         1
         click wishlist sort item         3
         sleep         1
         ${list}         get wishlist save list
         ${list_copy}         Copy List         ${list}
         Sort List         ${list_copy}
         Reverse List         ${list_copy}
         Should Be Equal         ${list}         ${list_copy}
         close

wishlist sort Price(low to high)
         open
         I Agree
         Sign In         zhuni.yang@razer.com         Razer123
         sleep         10
         change us
         Click Games
         add random game to wishlist
         sleep         1
         Click Wishlist
         click wishlist sort item         4
         sleep         2
         ${price_list}         get price list
         ${price_list_sort}         Copy List         ${price_list}
         sort List         ${price_list}
         Should Be Equal         ${price_list}         ${price_list_sort}
         close

wishlist sort Price(high to low)
         open
         I Agree
         Sign In         zhuni.yang@razer.com         Razer123
         sleep         10
         change us
         Click Games
         add random game to wishlist
         sleep         1
         Click Wishlist
         click wishlist sort item         5
         sleep         2
         ${price_list}         get price list
         ${price_list_sort}         Copy List         ${price_list}
         sort List         ${price_list}
         Reverse List         ${price_list}
         Should Be Equal         ${price_list}         ${price_list_sort}
         close
