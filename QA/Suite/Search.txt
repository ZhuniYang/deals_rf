*** Settings ***
Resource               ../Flow/Search.txt
Resource               ../base/base.txt
Library                Selenium2Library
Resource               ../Page/page_navigation.txt
Resource               ../Flow/User.txt
Library                ../MaxineLib/mytool.py
Resource               ../Page/page_search.txt
Resource               ../Page/page_bundles.txt
Resource               ../Page/page_games.txt
Library                Collections

*** Test Cases ***
header search game
         [Documentation]         验证header上的search game
         open
         click Games
         sleep         2
         ${serchkey}         get_random_text         //*[@id='dealsFiledLeft']//*[@class='games-ul']//*[@class='game-info']/p
         header_search         ${serchkey}
         sleep         2
         ${title}         get_first_text         xpath=//div[@class='game-info']/p
         Should Be Equal         ${serchkey}         ${title}
         close

header search bundle
         [Documentation]         验证header上的search bundle
         open
         Click Bundles
         ${serchkey}         get_random_text         xpath=//div[@class='title']
         header_search         ${serchkey}
         headersearch bundle
         sleep         2
         ${title}         get_first_text         xpath=//div[@class='title']
         Should Be Equal         ${serchkey}         ${title}
         close

games search
         [Documentation]         验证GAMES页面的search
         open
         Click Games
         ${search_key}         get random title
         games_search         ${search_key}
         sleep         3
         ${title}         get_first_text         xpath=//div[@class='game-info']/p
         ${equal_rate}         mytool.get_equal_rate         ${title}         ${search_key}
         ${equal_rate_float}         Evaluate         float(${equal_rate})
         ${rate}         Set Variable If         ${equal_rate_float}>0.7         True         False
         Should Be True         ${rate}
         close

bundles search
         [Documentation]         验证BUNDLES页面的search
         open
         Click Bundles
         sleep         2
         ${search_key}         get bundle random title
         bundle_search         ${search_key}
         sleep         2
         ${title}         get_first_text         xpath=//div[@class='title']
         Should Be Equal         ${search_key}         ${title}
         close
