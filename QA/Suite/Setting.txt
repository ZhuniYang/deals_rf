*** Settings ***
Library           Selenium2Library
Resource          ../base/base.txt
Resource          ../Flow/User.txt
Resource          ../Page/page_navigation.txt

*** Test Cases ***
connect gog
    open
    I Agree
    Sign In    mengli.wang@razer.com    12345678
    Click settings
    click    xpath=//*[@id="dealsFiledLeft"]/ul/li[2]
    sleep    3
    ${conn_button}    get_attr    xpath=//*[@id="dealsFiledRight"]/div/div[2]/button    class
    Run Keyword If    '${conn_button}'=='rui-btn rui-btn-gray-solid'    Run Keyword    Click Element    xpath=//*[@id="dealsFiledRight"]/div/div[2]/button/span
    ...    ELSE    Run Keyword    log    It can't connect
    sleep    3
    click    xpath=//*[@id="dealsFiledRight"]/div/div[2]/button/span    #点击connect按钮
    sleep    2
    Press Keys    xpath=/html/body/div[3]/div[2]/div/div/div[2]/div/div[3]/div/input    merryabc
    click    xpath=/html/body/div[3]/div[2]/div/div/div[3]/button[2]/span    #点击load wishlist
    sleep    30
    ${success_text}    Get Text    xpath=//*[@id="page-wrapper"]/div[1]/div[2]/div/div[1]
    Should Be Equal    ${success_text}    Wishlist import successfully
    close

connect humble bundle
    open
    I Agree
    Sign In    mengli.wang@razer.com    12345678
    Click settings
    click    xpath=//*[@id="dealsFiledLeft"]/ul/li[2]
    sleep    3
    ${conn_button}    get_attr    xpath=//*[@id="dealsFiledRight"]/div/div[3]/button    class
    Run Keyword If    '${conn_button}'=='rui-btn rui-btn-gray-solid'    Run Keyword    Click Element    xpath=//*[@id="dealsFiledRight"]/div/div[3]/button/span
    ...    ELSE    Run Keyword    log    It can't connect
    sleep    3
    click    xpath=//*[@id="dealsFiledRight"]/div/div[3]/button/span    #点击connect按钮
    sleep    2
    Press Keys    xpath=/html/body/div[3]/div[2]/div/div/div[2]/div/div[3]/div/input    6092142197080064
    click    xpath=/html/body/div[3]/div[2]/div/div/div[3]/button[2]/span    #点击load wishlist
    sleep    30
    ${success_text}    Get Text    xpath=//*[@id="page-wrapper"]/div[1]/div[2]/div/div[1]
    Should Be Equal    ${success_text}    Wishlist import successfully
    close

connect steam
    open
    I Agree
    Sign In    mengli.wang@razer.com    12345678
    Click settings
    click    xpath=//*[@id="dealsFiledLeft"]/ul/li[2]
    sleep    3
    ${conn_button}    get_attr    xpath=//*[@id="dealsFiledRight"]/div/div[1]/button    class
    Run Keyword If    '${conn_button}'=='rui-btn rui-btn-gray-solid'    Run Keyword    Click Element    xpath=//*[@id="dealsFiledRight"]/div/div[1]/button/span
    ...    ELSE    Run Keyword    log    It can't connect
    sleep    3
    click    xpath=//*[@id="dealsFiledRight"]/div/div[1]/button/span    #点击connect按钮
    sleep    10
    Press Keys    xpath=//*[@id="steamAccountName"]    merryabc
    Press Keys    xpath=//*[@id="steamPassword"]    aBc123456789
    click    xpath=//*[@id="imageLogin"]
    sleep    30
    ${success_text}    Get Text    xpath=//*[@id="page-wrapper"]/div[1]/div[2]/div[1]
    Should Contain    ${success_text}    The following games on your Wishlist are not available on Razer Game Deals:
    close
