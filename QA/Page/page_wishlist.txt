*** Settings ***
Library                String
Resource               ../base/base.txt
Library                Collections

*** Keywords ***
click random wishlist game
         [Documentation]         点击Wishlist页面的一个随机游戏
         ${ele_list}         Get WebElements         xpath=//ul[@class='wishlist-ul']//a
         ${lengh}         Get Length         ${ele_list}
         ${game_index}         evaluate         random.randint(0,${lengh}-1)         random
         click         ${ele_list}[${game_index}]

get wishlist logo list
         [Documentation]         获取Wishlist页面的logo list
         ${logo_list}         get_attr_list         xpath=//img[@class='platform']         src
         [Return]         ${logo_list}

click wishlist sort
         click         xpath=//button[@class='btn btn-gray-nohover dropdown-toggle sort-btn ']

click wishlist sort item - addtime
         [Arguments]         ${n}
         ${ele_list}         Get WebElements         xpath=//ul[@class='dropdown-menu download-menu pull-left sort-menu']//li
         click         ${ele_list}[${n}]

get wishlist first title
         [Documentation]         获取Wishlist页面第一个game的title
         ${first_title}         Get Text         xpath=//*[@id="dealsFiledLeft"]/ul/a[1]/li/div[2]/div[1]/div/p
         [Return]         ${first_title}

get wishlist discount list
         [Documentation]         返回Wishlist页面，discount的列表
         ${ele_list}         Get WebElements         xpath=//div[@class='discount-info']//div[@class='percent']
         ${discount_list}         Create List
         ${len}         Get Length         ${ele_list}
         : FOR         ${n}         IN RANGE         0         ${len}
         \         ${discount_string}         Get Text         ${ele_list}[${n}]
         \         ${discount}         Remove String         ${discount_string}         %         -
         \         ${discount_int}         Evaluate         int(${discount})
         \         Append To List         ${discount_list}         ${discount_int}
         [Return]         ${discount_list}

get wishlist save list
         [Documentation]         获取Wishlist页面的save值的列表
         ${ele_list}         Get WebElements         xpath=//div[@class='discount-info']//div[@class='percent']
         ${save_list}         Create List
         ${len}         Get Length         ${ele_list}
         : FOR         ${n}         IN RANGE         0         ${len}
         \         ${save_string}         Get Text         ${ele_list}[${n}]
         \         ${save}         Remove String         ${save_string}         -         $
         \         ${save_float}         Evaluate         float(${save})
         \         Append To List         ${save_list}         ${save_float}
         [Return]         ${save_list}

get wishlist price list
         ${price_list}         Create List
         ${ele_non_discount}         Get WebElements         xpath=//div[@class='item-price']
         ${ele_discount}         Get WebElements         xpath=//span[@class='price-new']
         ${ele}         Combine Lists         ${ele_non_discount}         ${ele_discount}
         ${len}         Get Length         ${ele}
         : FOR         ${n}         IN RANGE         0         ${len}
         \         ${text}         Get Text         ${ele}[${n}]
         \         ${price_string}         Set Variable If         '${text}'=='FREE!' or '${text}'=='To be released'         0         ${text}
         \         ${price}         Run Keyword If         '$' in \ '${price_string}'         Remove String         ${price_string}         $
         \         ...         AND         Evaluate         float(${price_string})
         \         ...         ELSE         Set Variable         0
         \         Append To List         ${price_list}         ${price}
         log         ${price_list}
         [Return]         ${price_list}

remove first wishlist game
         Mouse Over         xpath=//li[@class='dlc-game-li wishlist-li popInfo']
         wait visible         xpath=//*[@id='dealsFiledLeft']/ul/a[1]/li/div[4]/span
         click         xpath=//*[@id='dealsFiledLeft']/ul/a[1]/li/div[4]/span
