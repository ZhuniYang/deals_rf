*** Settings ***
Resource               ../base/base.txt
Resource               ../Page/page_game_filters_sort.txt
Resource               ../Page/page_bundles.txt
Resource               ../Page/page_wishlist.txt

*** Keywords ***
click games sort item
         [Arguments]         ${n}
         [Documentation]         点击games页面中的sort下的第n个选项
         click sort
         click sort item         ${n}

click bundles sort item
         [Arguments]         ${n}
         [Documentation]         bundle页面的sort选项流程
         ...         参数为n
         click bundle sort
         click bundle sort item - price         ${n}

click wishlist sort item
         [Arguments]         ${n}
         click wishlist sort
         click wishlist sort item - addtime         ${n}
